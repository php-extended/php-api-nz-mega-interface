<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

use Stringable;

/**
 * ApiNzMegaKeyAes256Interface interface file.
 *
 * Generic interface for manipulating 256 bits AES keys.
 *
 * @author Anastaszor
 */
interface ApiNzMegaKeyAes256Interface extends Stringable
{
	
	/**
	 * Gets a version of that key encoded in an array of eight 32 bits values.
	 *
	 * @return ApiNzMegaKeyAes256Interface
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function toArray32() : ApiNzMegaKeyAes256Interface;
	
	/**
	 * Gets a version of that key in pure form.
	 *
	 * @return ApiNzMegaKeyAes256Interface
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function toRawString() : ApiNzMegaKeyAes256Interface;
	
	/**
	 * Gets the 128 bits key which is hidden in the 256 bits.
	 *
	 * @return ApiNzMegaKeyAes128Interface
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function reduceAes128() : ApiNzMegaKeyAes128Interface;
	
	/**
	 * Gets the initialization vector hidden in the 256 bits.
	 *
	 * @return ApiNzMegaKeyAes128Interface
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function getInitializationVector() : ApiNzMegaKeyAes128Interface;
	
	/**
	 * Gets the meta mac data hidden in the 256 bits.
	 *
	 * @return ApiNzMegaKeyAes64Interface
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function getMetaMac() : ApiNzMegaKeyAes64Interface;
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

use DateTimeInterface;
use Stringable;

/**
 * ApiNzMegaResponseNodeInterface class file.
 *
 * This class represents a node, as given by the API.
 *
 * @author Anastaszor
 */
interface ApiNzMegaResponseNodeInterface extends Stringable
{
	
	/**
	 * Gets this node id.
	 *
	 * @return ApiNzMegaNodeIdInterface
	 */
	public function getNodeId() : ApiNzMegaNodeIdInterface;
	
	/**
	 * Gets the parent node id.
	 *
	 * @return ApiNzMegaNodeIdInterface
	 */
	public function getParentNodeId() : ApiNzMegaNodeIdInterface;
	
	/**
	 * Gets the owner user id.
	 *
	 * @return ApiNzMegaUserIdInterface
	 */
	public function getOwnerId() : ApiNzMegaUserIdInterface;
	
	/**
	 * Gets this node type.
	 *
	 * @return integer
	 */
	public function getNodeType() : int;
	
	/**
	 * Gets the attributes, as encrypted string.
	 *
	 * @return ApiNzMegaStringInterface
	 */
	public function getNodeAttributes() : ApiNzMegaStringInterface;
	
	/**
	 * Gets the key, as encrypted string.
	 *
	 * @return ApiNzMegaResponseKeyInterface
	 */
	public function getNodeKey() : ApiNzMegaResponseKeyInterface;
	
	/**
	 * Gets the node size, in octets.
	 *
	 * @return integer
	 */
	public function getNodeSize() : int;
	
	/**
	 * Gets the last modified date and time.
	 *
	 * @return DateTimeInterface
	 */
	public function getLastModifiedDatetime() : DateTimeInterface;
	
	/**
	 * Decodes the response node and gives a clear node.
	 *
	 * @param ApiNzMegaKeyAes128Interface $key
	 * @return ApiNzMegaNodeInterface
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function decode(ApiNzMegaKeyAes128Interface $key) : ApiNzMegaNodeInterface;
	
}

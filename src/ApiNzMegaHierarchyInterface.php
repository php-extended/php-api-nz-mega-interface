<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

use Stringable;

/**
 * ApiNzMegaHierarchyInterface class file.
 *
 * This class represents the hierarchy of nodes that are inside target folder.
 *
 * @author Anastaszor
 */
interface ApiNzMegaHierarchyInterface extends Stringable
{
	
	/**
	 * Gets the root node of the folder hierarchy.
	 *
	 * @return ApiNzMegaNodeInterface
	 */
	public function getRoot() : ApiNzMegaNodeInterface;
	
	/**
	 * Gets the node metadata for the node defined by given node id.
	 *
	 * @param ApiNzMegaNodeIdInterface $nodeId
	 * @return ?ApiNzMegaNodeInterface null if the node cannot be found
	 */
	public function get(ApiNzMegaNodeIdInterface $nodeId) : ?ApiNzMegaNodeInterface;
	
	/**
	 * Gets the children of the node defined by given node id.
	 *
	 * @param ApiNzMegaNodeIdInterface $nodeId
	 * @return array<integer, ApiNzMegaNodeInterface> or empty array if there is no children
	 * @throws ApiNzMegaExceptionInterface if the node cannot be found
	 */
	public function getChildren(ApiNzMegaNodeIdInterface $nodeId) : array;
	
}

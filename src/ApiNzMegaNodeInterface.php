<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

use DateTimeInterface;
use Stringable;

/**
 * ApiNzMegaNodeInterface class file.
 *
 * This class represents a node in mega's file and folders hierarchy. A node
 * represents metadata for a folder, a file, or other specific elements.
 *
 * @author Anastaszor
 */
interface ApiNzMegaNodeInterface extends Stringable
{
	
	/**
	 * Gets this node's id.
	 *
	 * @return ApiNzMegaNodeIdInterface
	 */
	public function getNodeId() : ApiNzMegaNodeIdInterface;
	
	/**
	 * Gets this node parent's id.
	 *
	 * @return ApiNzMegaNodeIdInterface
	 */
	public function getParentId() : ApiNzMegaNodeIdInterface;
	
	/**
	 * Gets this node owner's user id.
	 *
	 * @return ApiNzMegaUserIdInterface
	 */
	public function getOwnerId() : ApiNzMegaUserIdInterface;
	
	/**
	 * Gets the attributes for this node.
	 *
	 * @return ApiNzMegaAttributeInterface
	 */
	public function getAttributes() : ApiNzMegaAttributeInterface;
	
	/**
	 * Gets this node's type. Only one of ApiNzMegaNodeInterface::TYPE_* constants.
	 *
	 * @return integer
	 */
	public function getNodeType() : int;
	
	/**
	 * Gets the node size. Only set when nodes represents files.
	 *
	 * @return integer
	 */
	public function getNodeSize() : int;
	
	/**
	 * Gets the last modified date of this node.
	 *
	 * @return DateTimeInterface
	 */
	public function getLastModifiedDate() : DateTimeInterface;
	
	/**
	 * Gets the encryption key for this node.
	 *
	 * @return ApiNzMegaKeyAes128Interface
	 */
	public function getNodeKey() : ApiNzMegaKeyAes128Interface;
	
	/**
	 * Gets the initialization vector for this node.
	 *
	 * @return ApiNzMegaKeyAes128Interface
	 */
	public function getInitializationVector() : ApiNzMegaKeyAes128Interface;
	
	/**
	 * Gets the meta mac for this node.
	 *
	 * @return ?ApiNzMegaKeyAes64Interface
	 */
	public function getMetaMac() : ?ApiNzMegaKeyAes64Interface;
	
}

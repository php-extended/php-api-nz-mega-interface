<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

use Stringable;

/**
 * ApiNzMegaKeyAes128Interface interface file.
 *
 * Generic interface for manipulating 128 bits AES keys.
 *
 * @author Anastaszor
 */
interface ApiNzMegaKeyAes128Interface extends Stringable
{
	
	/**
	 * Gets a version of that key encoded in an array of four 32 bits values.
	 *
	 * @return ApiNzMegaKeyAes128Interface
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function toArray32() : ApiNzMegaKeyAes128Interface;
	
	/**
	 * Gets a version of that key in pure form.
	 *
	 * @return ApiNzMegaKeyAes128Interface
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function toRawString() : ApiNzMegaKeyAes128Interface;
	
}

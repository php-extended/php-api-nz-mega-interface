<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

use Psr\Http\Client\ClientExceptionInterface;
use RuntimeException;
use Stringable;

/**
 * ApiNzMegaEndpointInterface class file.
 *
 * This class represents the API to get resources and contents from mega urls.
 *
 * @author Anastaszor
 */
interface ApiNzMegaEndpointInterface extends Stringable
{
	
	/**
	 * Gets the root node of this folder.
	 *
	 * @return ApiNzMegaNodeInterface
	 * @throws ClientExceptionInterface
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function getRootNodeInfo() : ApiNzMegaNodeInterface;
	
	/**
	 * Gets file information from hierarchy, if not found, searches the server.
	 *
	 * @param ApiNzMegaNodeIdInterface $nodeId the id of target node
	 * @return ?ApiNzMegaNodeInterface the node with the given id, null if not found
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function getFileInfo(ApiNzMegaNodeIdInterface $nodeId) : ?ApiNzMegaNodeInterface;
	
	/**
	 * Gets the children of given node.
	 *
	 * @param ApiNzMegaNodeInterface $node
	 * @return array<integer, ApiNzMegaNodeInterface>
	 * @throws ClientExceptionInterface
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function getChildren(ApiNzMegaNodeInterface $node) : array;
	
	/**
	 * Gets the raw data for the file represented by given node.
	 *
	 * @param ApiNzMegaNodeInterface $node
	 * @return string binary raw data for the decoded file
	 * @throws ClientExceptionInterface
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function downloadFile(ApiNzMegaNodeInterface $node) : string;
	
	/**
	 * Downloads recursively all the files to the given filesystem, and stores
	 * them in clear, respecting the existing folder and file names that are
	 * stored in the mega data.
	 * 
	 * @param ApiNzMegaNodeInterface $node
	 * @param string $localPath
	 * @return integer the number of files downloaded
	 * @throws RuntimeException if data cannot be written on the filesystem
	 * @throws ClientExceptionInterface
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function downloadSynchronize(ApiNzMegaNodeInterface $node, string $localPath) : int;
	
}
